﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework05._06._18
{
    class Arrow : Shape
    {
        public override void Draw()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < Width - 4; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < i + 1; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for (int i = 0; i < Width; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < Width - 4; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 3; k > i; k--)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
