﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework05._06._18
{
    class Shape
    {
        public int height;
        public int width;
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                if (value <= 0 || value > 50)
                {
                    height = 5;
                }
                else
                {
                    height = value;
                }
            }
        }
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                if (value <= 0 || value > 50)
                {
                    width = 5;
                }
                else
                {
                    width = value;
                }
            }
        }

        public virtual void Draw()
        {
            Console.WriteLine();
        }
    }
}
