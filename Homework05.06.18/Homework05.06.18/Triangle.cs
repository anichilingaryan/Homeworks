﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework05._06._18
{
    class Triangle : Shape
    {
        public override void Draw()
        {
            for (int i = 1; i <= Height; i++)
            {
                for (int j = Height; j > i; j--)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < i; k++)
                {
                    Console.Write("*");
                }
                for (int n = 1; n < i; n++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
