﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework05._06._18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Shape l = new Line { Width = 10 };
            //l.Draw();

            //Shape ar = new Arrow { Width = 15 };
            //ar.Draw();

            //Shape t = new Triangle { Height = 10, Width = 10};
            //t.Draw();

            //Shape r = new Rectangle { Height = 5, Width = 10 };
            //r.Draw();

            List<Shape> shapes = new List<Shape>();
            shapes.Add(new Line { Width = 10 });
            shapes.Add(new Arrow { Width = 15 });
            shapes.Add(new Triangle { Height = 10, Width = 10 });
            shapes.Add(new Rectangle { Height = 5, Width = 10 });

            foreach (var item in shapes)
            {
                item.Draw();
            }
            Console.ReadLine();
        }
    }
}
