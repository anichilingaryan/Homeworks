﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework05._06._18
{
    class Line : Shape
    {
        public override void Draw()
        {
            for (int i = 0; i < Width; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
}
