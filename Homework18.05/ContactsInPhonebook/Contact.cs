﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInPhonebook
{
    class Contact
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }

        public void Print()
        {
            Console.WriteLine($"{Number}, {Name}, {Surname}, {Company}, {Address}");
        }

    }
}
