﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInPhonebook
{
    class Phonebook
    {
        Dictionary<string, Contact> dict = new Dictionary<string, Contact>();
        public void AddContact(Contact c)
        {
            dict.Add(c.Number, c);
        }

        public void RemoveContact(string number)
        {
            dict.Remove(number);
        }
        public void RemoveContact(Contact c)
        {
            RemoveContact(c.Number);
        }

        public Contact FindContact(string number)
        {
            if (dict.ContainsKey(number))
            {
                return dict[number];
            }
            
            return null;
        }

        public void Print()
        {
            foreach (var item in dict)
            {
                item.Value.Print();
            }
        }
    }    
}
