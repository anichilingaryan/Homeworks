﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInPhonebook
{
    class Program
    {
        static void Main(string[] args)
        {
            Phonebook phonebook = new Phonebook();

            Contact contact1 = new Contact()
            {
                Number = "+37491000001",
                Name = "A1",
                Surname = "A1yan",
                Company = "A1Company",
                Address = "A1yan street 01",
            };

            Contact contact2 = new Contact()
            {
                Number = "+37491000002",
                Name = "A2",
                Surname = "A2yan",
                Company = "A2Company",
                Address = "A2yan street 02",
            };

            Contact contact3 = new Contact()
            {
                Number = "+37491000003",
                Name = "A3",
                Surname = "A3yan",
                Company = "A3Company",
                Address = "A3yan street 03",
            };

            phonebook.AddContact(contact1);
            phonebook.AddContact(contact2);
            phonebook.AddContact(contact3);
            phonebook.Print();

            Contact contact = phonebook.FindContact("+37491000001");
            contact.Print();

            phonebook.RemoveContact(contact2);
            phonebook.Print();
            Console.ReadLine();
        }
    }
}
