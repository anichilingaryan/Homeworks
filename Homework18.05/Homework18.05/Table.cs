﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework18._05
{
    class Table
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public void Draw()
        {
            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Column; j++)
                {
                    Console.Write(new string('-', 5));
                }
                Console.WriteLine();
                for (int k = 0; k < Column*5 + 1; k++)
                {
                    if ((k == 0) || k % 5 == 0)
                    {
                        Console.Write('|');
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
            for (int i = 0; i < Column; i++)
            {
                Console.Write(new string('-', 5));
            }
            Console.WriteLine();
        }
    }
}
