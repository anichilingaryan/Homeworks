﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework18._05
{
    class Program
    {
        static void Main(string[] args)
        {
            Table table = new Table { Row = 6, Column = 10 };
            table.Draw();

            Console.ReadLine();
        }
    }
}
